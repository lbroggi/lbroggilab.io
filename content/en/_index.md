---
title: "Luca Broggi"

description: "Astrophysicist"
cascade:
  featured_image: '/images/TDE_background.jpg'
---

I am an astrophysicist. Currently a Postdoctoral Researcher at [Università degli Studi di Milano-Bicocca](https://en.unimib.it/), in the group of prof. Alberto Sesana.
