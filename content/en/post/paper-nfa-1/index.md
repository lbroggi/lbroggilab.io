---
date: 2023-03-08
description: ""
featured_image: "BG.png"
tags: ["scene"]
title: "Partial stellar tidal disruption events and their rates "
---

![Title and authors](paper-nfa-1.png)

*Bortolas, Elisa; Ryu, Taeho; **Broggi, Luca**; Sesana, Alberto.*

Tidal disruption events (TDEs) of stars operated by massive black holes (MBHs) will be detected in thousands by upcoming facilities such as the Vera Rubin Observatory. In this work, we assess the rates of standard total TDEs, destroying the entire star, and partial TDEs, in which a stellar remnant survives the interaction, by solving 1-D Fokker-Planck equations. Our rate estimates are based on a novel definition of the loss cone whose size is commensurate to the largest radius at which partial disruptions can occur, as motivated by relativistic hydrodynamical simulations. Our novel approach unveils two important results. First, partial TDEs can be more abundant than total disruptions by a factor of a few to a few tens. Second, the rates of complete stellar disruptions can be overestimated by a factor of a few to a few tens if one neglects partial TDEs, as we find that many of the events classified at total disruptions in the standard framework are in fact partial TDEs. Accounting for partial TDEs is particularly relevant for galaxies harbouring a nuclear stellar cluster featuring many events coming from the empty loss cone. Based on these findings, we stress that partial disruptions should be considered when constraining the luminosity function of TDE flares; accounting for this may reconcile the theoretically estimated TDE rates with the observed ones. 

![PTDE rates vs Full TDE rates](Content-1.png)

[ArXiv](https://ui.adsabs.harvard.edu/link_gateway/2023arXiv230303408B/EPRINT_PDF)

<!-- [Published Version](https://doi.org/10.1093/mnras/stac1453) -->