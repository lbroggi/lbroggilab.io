---
date: 2024-11-06
description: ""
featured_image: "BG.png"
tags: ["scene"]
title: "Breaking boundaries: extending the orbit averaged Fokker-Planck equation inside the loss cone"
---

![Breaking boundaries: extending the orbit averaged Fokker-Planck equation inside the loss cone](paper-fa-3.png)

**Broggi, Luca**

In this letter, we present a new formulation of loss cone theory as a reaction-diffusion system, which is orbit averaged and accounts for loss cone events through a sink term. This formulation can recover the standard approach based on boundary conditions, and is derived from a simple physical model that overcomes many of the classical theoretical constraints. The relaxed distribution of disruptive orbits in phase space has a simple analytic form, and it predicts accurately the pericentre of tidal disruption events at disruption, better than other available formulas. This formulation of the problem is particularly suitable for including more physics in tidal disruptions and the analogous problem of gravitational captures, e.g. strong scatterings, gravitational waves emission, physical stellar collisions, and repeating partial disruptions - that can all act on timescale shorter than two-body relaxation. This allows to explore in a simple way dynamical effects that might affect tidal disruption events rates, tackling the expected vs observed rate tension and the over-representation of E+A galaxies.


![Rates of TDEs, EMRIs and Plunges in time](Content-1.png)

[ArXiv](https://ui.adsabs.harvard.edu/link_gateway/2024arXiv241104178B/EPRINT_PDF)

<!-- [Published Version]() -->
