---
date: 2023-12-20
description: ""
featured_image: "BG.png"
tags: ["scene"]
title: "Demographics of Tidal Disruption Events with L-Galaxies: I. Volumetric TDE rates and the abundance of Nuclear Star Clusters"
math: true
---

![Title and authors](paper-nfa-3.png)

*Polkas, Markos; Bonoli, Silvia; Bortolas, Elisa; Izquierdo-Villalba, David; Sesana, Alberto; **Broggi, Luca**; Hoyer, Nils; Spinoso, Daniele*

Stars can be ripped apart by tidal forces in the vicinity of a massive black hole, causing luminous flares classified as tidal disruption
events (TDEs). These events could be contributing to the mass growth of intermediate-mass black holes, while new samples from
ongoing transient surveys can provide useful information on this unexplored growth channel. This work aims to study the demo-
graphics of TDEs by modeling the co-evolution of black holes and their galactic environments in a cosmological framework. We use
the semi-analytic galaxy formation model L-Galaxies, which follows the evolution of galaxies as well as of massive black holes,
including multiple scenarios for black hole seeds and growth, spin evolution, and binary black hole dynamics. Time-dependent TDE
rates are associated with each black hole depending on the stellar environment, following the solutions to the 1-D Fokker Planck
equation solved with PhaseFlow. Our model produces TDE volumetric rates that are in agreement with the latest optical sample of
33 TDEs and previous X-ray samples. This agreement requires a high occupation fraction of nuclear star clusters with black holes
since these star reservoirs host the majority of TDEs at all mass regimes. Unlike previous studies, we predict that TDE rates are an
increasing function of black hole mass up to a peak of ∼ \(10^{6.5} \, M_\odot\), beyond which rates drop following a shallow power-law distribu-
tion. In general, rates are predicted to be redshift-independent at z < 1. Yet, the TDE rates in active black holes of different levels of
activity may evolve with redshift. We also discuss implications for the black hole spin distribution at the event horizon suppression and
the cumulative growth due to TDEs. Our results highlight the need for time-dependent TDE rates, especially towards the low-mass
regimes of the intermediate-mass black holes and dwarf galaxies.

![GW Background for LISA and mu-Ares](Content-1.png)

[ArXiv](https://ui.adsabs.harvard.edu/link_gateway/2023arXiv231213242P/EPRINT_PDF)

<!-- [Published Version]() -->
