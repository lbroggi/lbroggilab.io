---
date: 2024-09-13
description: ""
featured_image: "BG.png"
tags: ["scene"]
title: "Hanging on the cliff: EMRI formation with local two-body relaxation and post-Newtonian dynamics "
math: true
---

![Hanging on the cliff: EMRI formation with local two-body relaxation and post-Newtonian dynamics ](paper-nfa-4.png)

*Mancieri, Davide; **Broggi, Luca**; Bonetti, Matteo; Sesana, Alberto*

Extreme mass ratio inspirals (EMRIs) are anticipated to be primary gravitational wave sources for the Laser Interferometer Space
Antenna (LISA). They form in dense nuclear clusters, when a compact object is captured by the central massive black holes (MBHs)
as a consequence of the frequent two-body interactions occurring between orbiting objects. The physics of this process is complex and
requires detailed statistical modeling of a multi-body relativistic system. We present a novel Monte Carlo approach to evolve the post-
Newtonian (PN) equations of motion of a compact object orbiting an MBH accounting for the effects of two-body relaxation locally
on the fly, without leveraging on the common approximation of orbit-averaging. We apply our method to study the function \(S (a_0)\),
describing the fraction of EMRI to total captures (including EMRIs and direct plunges, DPs) as a function of the initial semi-major
axis a0 for compact objects orbiting central MBHs with \(M_\bullet \in [10^4 M_\odot, 4 \times 10^6 M_\odot]\). The past two decades consolidated a picture in
which \(S (a_0) \to 0\) at large initial semi-major axes, with a sharp transition from EMRI to DPs occurring around a critical scale ac. A
recent study challenges this notion for low-mass MBHs, finding EMRIs forming at \(a \gg a_c\) which were named “cliffhangers”. Our
simulations confirm the existence of cliffhanger EMRIs, which we find to be more common then previously inferred. Cliffhangers
start to appear for \( M_\bullet \lesssim 3 \times 10^5 M_\bullet\) and can account for up to 55% of the overall EMRIs forming at those masses. We find \(S (a_0) \ll 0\)
for \(a \ll a_c\), reaching values as high as 0.6 for \(M_\bullet = 10^4 M_\odot\), much larger than previously found. We test how these results are
influenced by different assumptions on the dynamics used to evolve the system and treatment of two-body relaxation. We find that the
PN description of the system greatly enhances the number of EMRIs by shifting ac to larger values at all MBH masses. Conversely,
the local treatment of relaxation has a mass dependent impact, significantly boosting the number of cliffhangers at small MBH masses
compared to an orbit-averaged treatment. These findings highlight the shortcomings of standard approximations used in the EMRI
literature and the importance of carefully modeling the (relativistic) dynamics of these systems. The emerging picture is more complex
than previously thought, and should be considered in future estimates of rates and properties of EMRIs detectable by LISA

![GW Background for LISA and mu-Ares](Content-1.png)

[ArXiv](https://ui.adsabs.harvard.edu/link_gateway/2024arXiv240909122M/EPRINT_PDF)

<!-- [Published Version]() -->
