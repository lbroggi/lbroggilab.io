---
date: 2024-04-10
description: ""
featured_image: "BG.png"
tags: ["scene"]
title: "Repeating partial disruptions and two-body relaxation"
---

![Repeating TDEs and 2-body relaxation](paper-fa-2.png)

***Broggi, Luca**; Stone, Nicholas C.; Ryu, Taeho; Bortolas, Elisa; Dotti, Massimo; Bonetti, Matteo; Sesana, Alberto*

Two-body relaxation may drive stars onto near-radial orbits around a massive black hole, resulting in
a tidal disruption event (TDE). In some circumstances, stars are unlikely to undergo a single terminal
disruption, but rather to have a sequence of many grazing encounters with the black hole. It has
long been unclear what is the physical outcome of this sequence: each of these encounters can only
liberate a small amount of stellar mass, but may significantly alter the orbit of the star. We study
the phenomenon of repeating partial tidal disruptions (pTDEs) by building a semi-analytical model
that accounts for mass loss and tidal excitations. In the empty loss cone regime, where two-body
relaxation is weak, we estimate the number of consecutive partial disruption that a star can undergo,
on average, before being significantly affected by two-body encounters. We find that in this empty loss
cone regime, a star will be destroyed in a sequence of weak pTDEs, possibly explaining the tension
between the low observed TDE rate and its higher theoretical estimates.

![Rates of TDEs, EMRIs and Plunges in time](Content-1.png)

[ArXiv](https://ui.adsabs.harvard.edu/link_gateway/2024arXiv240405786B/EPRINT_PDF)

[Published Version](https://astro.theoj.org/article/120086-repeating-partial-disruptions-and-two-body-relaxation)

<!-- [Published Version]() -->
