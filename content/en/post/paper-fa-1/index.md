---
date: 2022-05-12
description: ""
featured_image: "BG.png"
tags: ["scene"]
title: "Extreme mass ratio inspirals and tidal disruption events in nuclear clusters - I. Time-dependent rates"
---

![Rates of TDEs, EMRIs and Plunges in time](paper-fa-1.png)

***Broggi, Luca**; Bortolas, Elisa; Bonetti, Matteo; Sesana, Alberto; Dotti, Massimo.*

In this paper, we develop a computationally efficient, two-population, time-dependent Fokker–Plank approach in the two dimensions of energy and angular momentum to study the rates of tidal disruption events (TDEs), extreme mass ratio inspirals (EMRIs), and direct plunges occurring around massive black holes (MBHs) in galactic nuclei. We test our code by exploring a wide range of the astrophysically relevant parameter space, including MBH masses, galaxy central densities, and inner density slopes. We find that mass segregation and, more in general, the time depend ency of the distribution function regulate the event rate: TDEs always decline with time, whereas EMRIs and plunges reach a maximum and undergo a subsequent nearly exponential decay. Once suitably normalized, the rates associated to different choices of MBH mass and galaxy density overlap nearly perfectly. Based on this, we provide a simple scaling that allows to reproduce the time-dependent event rates for any MBH mass and underlying galactic nucleus. Although our peak rates are in general agreement with the literature relying on the steady-state (non-time-dependent) assumption, those can be sustained on a time-scale that strongly depends on the properties of the system. In particular, this can be much shorter than a Gyr for relatively light MBHs residing in dense systems. This warns against using steady-state models to compute global TDE, EMRI, and plunge rates and calls for a more sophisticated, time-dependent treatment of the problem.

My Fokker-Planck code is open source, you can find it here [gitlab.com/j2970/juliafokkerplanck](https://gitlab.com/j2970/juliafokkerplanck).

![Rates of TDEs, EMRIs and Plunges in time](Rates.png)

[ArXiv](https://ui.adsabs.harvard.edu/link_gateway/2022MNRAS.514.3270B/EPRINT_PDF)

[Published Version](https://doi.org/10.1093/mnras/stac1453)