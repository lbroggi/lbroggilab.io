---
date: 2023-03-24
description: ""
featured_image: "BG.png"
tags: ["scene"]
title: "Gravitational waves from an eccentric population of primordial black holes orbiting Sgr A⋆ "
math: true
---

![Title and authors](paper-nfa-2.png)

*Bondani, Stefano; Bonetti, Matteo; **Broggi, Luca**; Haardt, Francesco; Sesana, Alberto; Dotti, Massimo.*

Primordial black holes (PBH), supposedly formed in the very early Universe, have been proposed as a possible viable dark matter candidate. In this work we characterize the expected gravitational wave (GW) losses from a population of PBHs orbiting Sgr A⋆, the super-massive black hole at the Galactic center, and assess the signal detectability by the planned space-borne interferometer LISA and by the proposed next generation space-borne interferometer μAres. Assuming that PBHs indeed form the entire diffuse mass allowed to reside within the orbit of the S2 star, we compute an upper limit to the expected GW signal both from resolved and non-resolved sources, under the further assumptions of monochromatic mass function and thermally distributed eccentricities. By comparing with our previous work where PBHs on circular orbits were assumed, we show how the GW signal from high harmonics over a 10 year data stream increases by a factor of six the chances of LISA detectability, from the ≈10% of the circular case, to ≈60%, whereas multiple sources can be identified in 20% of our mock populations. The background signal, made by summing up all non resolved sources, should be detectable thanks to the PBHs with higher eccentricity evolving under two body relaxation. In the case of μAres, because of its improved sensitivity in the μHz band, one third of the entire population of PBHs orbiting Sgr A⋆ would be resolved. The background noise from the remaining non resolved sources should be detectable as well.

![GW Background for LISA and mu-Ares](Content-1.png)

[ArXiv](https://ui.adsabs.harvard.edu/link_gateway/2023arXiv230312868B/EPRINT_PDF)

[Published Version](https://ui.adsabs.harvard.edu/link_gateway/2024PhRvD.109d3005B/doi:10.1103/PhysRevD.109.043005)
