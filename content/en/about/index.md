---
title: "About"
description: "I am a young astrophysicist, currently fascinated by the theorical and computational side."
featured_image: 'BG.png'
---

 ![Picture of me](Luca.jpg)

In my research, I focus on mean field models of the nuclei of galaxies.  I am developing a public code to simulate the evolution of these systems over cosmic timescales that does not rely on effective, approximate equations.

More generally, I am interested in the dynamical origin of gravitational wave sources and the statistical mechanics of gravitating systems.

And, finally, I am a fan of rollercoasters and Doctor WHO.

## Current position

**Postdoc researcher**, Università degli Studi di Milano-Bicocca

## Education

**Phd in Physics & Astronomy**, Università degli Studi di Milano-Bicocca

**MSc in Theoretical & Computational Physics**, Università degli Studi di Milano-Bicocca

**BSc in Physics**, Università degli Studi di Milano-Bicocca

## Detailed info

|  |  |  |
|--:|--|:--|
| **Full CV** | \(\quad\) | {{< download string="Download" link="LucaBroggi-full-CV.pdf" filename="LucaBroggi-fullCV.pdf">}} |
| **Short CV** | \(\quad\) | {{< download string="Download" link="LucaBroggi-short-CV.pdf" filename="LucaBroggi-shortCV.pdf">}} |
| **Publication List** | \(\quad\) | [View the NASA/ADS library](https://ui.adsabs.harvard.edu/public-libraries/xB-ITUDfTr2FoqWpgUzzcw)|
| **ORCID** | \(\quad\) | [0000-0002-9076-1094](https://orcid.org/0000-0002-9076-1094) |
